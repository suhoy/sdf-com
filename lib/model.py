from enum import Enum

HEADER = 'acdc'
TEXT_CODEC = 'UTF'
TEXT_ENCODING = 'utf-8'


class Mode(Enum):
    receiver = 'receiver'
    sender = 'sender'

    def __str__(self):
        return self.value


class DataType(Enum):
    text = 'T'
    audio = 'A'

    def __str__(self):
        return self.value


class Encryption(Enum):
    plain = 'P'
    encrypted = 'E'

    def __str__(self):
        return self.value


def _pad(original: bytearray, size: int):
    base = bytearray(size)
    base = base[:(size - len(original))]
    return base + original


class Packet:
    def __init__(self, metadata: str, seq: int, payload: bytearray):
        """
        Class representing the packet structure
        :param metadata: 5 bytes -> ' (T|A) (E|P) (000|WAV|FLV|...) '
        :param seq: 4 bytes -> sequence number of the packet
        :param payload: data of the size of the buffer
        """
        self.payload = payload
        self.meta = metadata
        self.seq = seq

    def is_encrypted(self):
        return Encryption(self.meta[1]) == Encryption.encrypted

    def to_bytes(self):
        header = bytearray.fromhex(HEADER)
        meta_data_arr = self.meta.encode(TEXT_ENCODING)

        seq = _pad(bytearray([self.seq]), 4)
        length = _pad(bytearray([len(self.payload)]), 2)
        clear = bytearray('\n'.encode('utf-8'))
        packet = header + meta_data_arr + seq + length + self.payload + clear

        return packet

    @staticmethod
    def from_bytes(byte_array: bytearray):
        header = byte_array[:2]
        assert header == bytearray.fromhex(HEADER)

        metadata = byte_array[2:7]
        seq = byte_array[7:11]
        length = int(byte_array[11:13].hex(), 16)
        payload = byte_array[13:]

        assert len(payload) == length

        return Packet(
            metadata.decode(TEXT_ENCODING),
            int(seq.hex(), 16),
            payload
        )
