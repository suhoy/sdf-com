import logging
import time
import serial

from lib.model import TEXT_ENCODING, DataType, Packet, Encryption, TEXT_CODEC

LOGGER = logging.getLogger(__name__)


####################################################
# Text

def _input_text(payload_size: int) -> list:
    s = input("[secret-messaging]> ")

    total = bytearray(s.encode(TEXT_ENCODING))
    payloads = []

    while len(total) > payload_size:
        payloads.append(total[:payload_size])
        total = total[payload_size:]

    payloads.append(total)
    return payloads


def _output_text(payloads: list):
    message = ''
    for payload in payloads:
        message += payload.decode(TEXT_ENCODING)
    print(message)


####################################################
# Audio

def _input_audio(buffer_size: int) -> list:
    pass


def _output_audio(payload):
    pass


####################################################
# Encryption

def _xor_with_key(payload: bytearray, key: str) -> bytearray:
    key_arr = key.encode(TEXT_ENCODING)
    result = bytearray()

    for i in range(len(payload)):
        result.append(payload[i] ^ key_arr[i % len(key)])

    return result


####################################################

class TransceiverFactory:
    def __init__(self, data_type: DataType, encryption_key, serial_port: str, rate: int, connection=None):

        # Setting Encryption Settings
        self.encryption_enabled = encryption_key is not None and encryption_key != ''
        self.key = encryption_key

        # Setting Metadata
        enc_tag = Encryption.encrypted if self.encryption_enabled else Encryption.plain

        encoding = '000'
        if data_type == data_type.text:
            encoding = TEXT_CODEC

        self.metadata = '{}{}{}'.format(data_type.value, enc_tag, encoding)
        self.serial_port = serial_port

        # Setting i/o functions
        self._input = _input_text if data_type == DataType.text else _input_audio
        self._output = _output_text if data_type == DataType.text else _output_audio

        if connection is None:
            self._conn = serial.Serial(self.serial_port, rate)
        else:
            self._conn = connection

    def receive(self):
        running = True
        results = []
        while running:

            try:
                raw = self._conn.readline()[:-1]
                if raw == b'':
                    continue
                packet = Packet.from_bytes(raw)
                payload = packet.payload

                if packet.is_encrypted() and self.key:
                    payload = _xor_with_key(payload, self.key)

                LOGGER.info('Received {}'.format(packet.to_bytes()))

                if packet.seq == 0:
                    if len(results) > 0:
                        self._output(results[len(results) - 1])

                    new_seq = [payload]
                    results.append(new_seq)
                else:
                    results[len(results) - 1].append(payload)

            except KeyboardInterrupt:
                running = False

    def send(self, payload_size: int):
        running = True
        while running:

            try:
                input_payloads = self._input(payload_size)

                seq = 0

                for payload in input_payloads:
                    if self.encryption_enabled:
                        payload = _xor_with_key(payload, self.key)

                    packet = Packet(self.metadata, seq, payload)
                    seq += 1

                    LOGGER.info('Sending {}'.format(packet.to_bytes()))
                    self._conn.write(packet.to_bytes())
                    time.sleep(1)
                    print('DEVICE > ', self._conn.read_all())

            except KeyboardInterrupt:
                running = False
