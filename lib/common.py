import argparse
import logging

from lib.model import DataType, Mode

DEFAULT_RATE = 9600


def init_logger(level):
    logger = logging.getLogger()

    handler = logging.StreamHandler()

    formatter = logging.Formatter(
        fmt='[ %(asctime)s ] :: %(levelname)5s :: ( %(module)s ) -- %(message)s')
    handler.setFormatter(formatter)

    logger.addHandler(handler)

    if level == 1:
        logger.setLevel(logging.INFO)
    elif level > 1:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.WARN)


def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument('-v', '--verbose',
                        action='count',
                        default=0,
                        help='Increase level of output sent to stderr')

    parser.add_argument('-k', '--key',
                        help='Key used to encrypt the communication',
                        default='')

    parser.add_argument('-s', '--serial-port',
                        help='Serial Port of the USB connected to the Arduino',
                        default='/dev/ttyUSB0')

    parser.add_argument('-r', '--rate',
                        help='Serial Port of the Arduino',
                        default=DEFAULT_RATE)

    parser.add_argument('-b', '--payload-size',
                        help='Size of the packet\'s payload in bytes',
                        default=60)

    parser.add_argument('-m', '--mode',
                        help='Communication Type',
                        type=Mode, choices=list(Mode),
                        default='sender')

    parser.add_argument('-d', '--data-type',
                        help='Data Type',
                        type=DataType, choices=list(DataType),
                        default='T')

    return parser.parse_args()
