#!/usr/bin/env python3
import logging
import sys
import time

from lib.common import parse_args, init_logger
from lib.model import Mode
from lib.transceiver import TransceiverFactory

LOGGER = logging.getLogger(__name__)


def main():
    try:
        opts = parse_args()
        init_logger(opts.verbose)

        key = opts.key
        serial_port = opts.serial_port
        payload_size = int(opts.payload_size)
        mode = opts.mode
        data_type = opts.data_type
        rate = int(opts.rate)

        transceiver = TransceiverFactory(data_type, key, serial_port,rate)

        LOGGER.info('Starting the transceiver...')
        time.sleep(1);
        print(transceiver._conn.read_all())

        if mode == Mode.sender:
            transceiver.send(payload_size)
        elif mode == Mode.receiver:
            transceiver.receive()
    except EOFError:
        print("EOF: done");
        time.sleep(1);
        print(transceiver._conn.read_all())
        sys.exit(0)
    except KeyboardInterrupt as e:
        LOGGER.info('Force Stopping the Recorder')
        LOGGER.debug('Existing with error: {}'.format(e))
        sys.exit(0)
    else:
        LOGGER.info('Stopping the Recorder')


if __name__ == '__main__':
    main()
