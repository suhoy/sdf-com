#include <RCSwitch.h>

RCSwitch mySwitch = RCSwitch();


String inputString = "";         // a String to hold incoming data
bool stringComplete = false;  // whether the string is complete

void setup() {
  // initialize serial:
  Serial.begin(9600);
  // reserve 200 bytes for the inputString:
  inputString.reserve(200);
  mySwitch.enableTransmit(10);
}

void loop() {
  // print the string when a newline arrives:
  if (stringComplete) {


    Serial.println(inputString);
    send_string(inputString.c_str());
    // clear the string:
    inputString = "";
    stringComplete = false;
  }
}


void send_string(const char *str)
{
  int i = 0;
  for (char *p = str; *p; p++) {
      mySwitch.send((i<<8) + *p, 16);
      i++;
  }
}

void serialEvent() {

  while (Serial.available()) {
    char inChar = (char)Serial.read(); // maybe remove char ?

    digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(500);                       // wait for a second
    digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
    delay(500);

    inputString += inChar;
    if (inChar == '\n') {

      stringComplete = true;
    }
  }
}