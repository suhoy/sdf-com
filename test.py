from lib.model import Packet, DataType
from lib.transceiver import TransceiverFactory


class MockSerial:
    def __init__(self):
        self.raw = [
            bytearray(b'\xac\xdcTP000\x00\x00\x00\x00\x00\x08asdasd a'),
            bytearray(b'\xac\xdcTP000\x00\x00\x00\x01\x00\x08sd asd a'),
            bytearray(b'\xac\xdcTP000\x00\x00\x00\x02\x00\x06sd asd'),
        ]
        self.i = 0

    def readline(self):
        arr = self.raw[self.i]
        self.i = (self.i + 1) % len(self.raw)
        return arr

    def write(self, byte_arr):
        self.raw.append(byte_arr)
        print(byte_arr)


p = Packet('TP000', 0, bytearray('testing'.encode('utf-8')))
s = Packet.from_bytes(p.to_bytes())

assert s.to_bytes() == p.to_bytes()

transceiver = TransceiverFactory(DataType.text, None, '', 0, MockSerial())

# transceiver.receive()
transceiver.send(8)

